# MenuNavigator

Vaadin 10 Java integration

## Development instructions

Starting the test/demo server:
```
mvn jetty:run
```

This deploys demo at http://localhost:9094

### Branching information

* `master` the latest version of the starter, using latest platform snapshot
* `V10` the version for Vaadin 10
* `V11` the version for Vaadin 11
