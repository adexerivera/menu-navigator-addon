package org.itccanarias.dcct;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.theme.lumo.Lumo;
import org.itccanarias.dcct.components.*;
import org.itccanarias.dcct.utils.MenuUtil;

import java.util.List;

@Route("")
@HtmlImport("frontend://styles/mn-styles.html")
@HtmlImport("frontend://styles/mn-styles.html")
public class DemoView extends HorizontalLayout {

    public DemoView() {

        MenuUtil menuUtil = new MenuUtil();
        menuUtil.load();
        List<MenuItem> leftPages = menuUtil.getLeftMenuItems();

        // Left menu example
        LeftMenu leftMenu = new LeftMenu();
        leftMenu.add(new Label("Menu Navigator"));

        for (MenuItem page : leftPages) {
            String stringIcon = (page.getIcon() == null) ? "LINES_LIST" : page.getIcon().toUpperCase();
            VaadinIcon vIcon = VaadinIcon.valueOf(stringIcon);
            leftMenu.add(MenuNavButton.get()
                    .withCaption(page.getTitle())
                    .withIcon(vIcon)
                    .withNavigateTo(page.getLink()));
        }

        SubMenu demoSettings = leftMenu.add(SubMenu.get()
                .withCaption("Settings")
                .withIcon(VaadinIcon.COGS));

        demoSettings.add(MenuNavButton.get()
                .withCaption("White Theme")
                .withIcon(VaadinIcon.PALETE)
                .withClickListener(e -> UI.getCurrent().getElement().setAttribute("theme", "")));

        demoSettings.add(MenuNavButton.get()
                .withCaption("Dark Theme")
                .withIcon(VaadinIcon.PALETE)
                .withClickListener(e -> UI.getCurrent().getElement().setAttribute("theme", Lumo.DARK)));

        add(leftMenu);

        // Tool bar example
        VerticalLayout vl = new VerticalLayout();

        List<MenuItem> toolbarItems = menuUtil.getToolbarMenuItems();

        Toolbar toolbarIcons = new Toolbar(toolbarItems);
        toolbarIcons.getToolbarMenu().setWidth("100%");
        vl.add(new H1("Tool bar with icons"));
        vl.add(toolbarIcons);

        Toolbar toolbarWithoutIcons = new Toolbar();
        toolbarWithoutIcons.setUseIcons(false);
        toolbarWithoutIcons.getToolbarMenu().setWidth("100%");
        toolbarWithoutIcons.setToolbarMenuItems(toolbarItems);
        vl.add(new H1("Tool bar without icons"));
        vl.add(toolbarWithoutIcons);

        // Board example
        Board board = new Board(menuUtil.getDashboardMenuItems(), "Demo");
        vl.add(board);

        add(vl);
    }
}
