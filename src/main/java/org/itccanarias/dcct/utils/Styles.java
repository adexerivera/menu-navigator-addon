package org.itccanarias.dcct.utils;

public class Styles {
    /* CSS Classes */
    public static final String leftMenu = "mn-left-menu";
    public static final String leftMenuItem = "mn-left-menu-item";
    public static final String toolbar = "mn-toolbar-menu-container";
    public static final String toolbarMenu = "mn-toolbar-menu";
    public static final String toolbarMenuItem = "mn-toolbar-menu-item";
    public static final String subMenu = "mn-sub-menu";
    public static final String toolTip = "mn-tool-tip";
    public static final String buttonOtherIcon = "mn-btn-other-icon";
    public static final String board = "mn-board-container";
    public static final String boardCard = "mn-board-card";

    /* General */
    public static final String active = "active";
    public static final String open = "open";
}
