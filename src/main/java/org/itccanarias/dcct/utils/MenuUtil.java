package org.itccanarias.dcct.utils;

import org.codehaus.jackson.map.ObjectMapper;
import org.itccanarias.dcct.components.MenuItem;
import org.springframework.context.annotation.Configuration;
import org.yaml.snakeyaml.Yaml;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author arivera on 14/01/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 * <p>
 * This utility read menu.yml file and create menu items for left, header, dashboard and admin navigations
 * there are some menu.yml.ENVIRONMENT files to copy and create menu.yml file.
 */
@Configuration
public class MenuUtil {

    private List<MenuItem> menus = new ArrayList<>();

    public void load() {
        load("menu.yml");
    }

    public void load(String resourcePath) {
        // Load menu configuration from menu.yml
        Yaml yaml = new Yaml();
        InputStream inputStream = this.getClass()
                .getClassLoader()
                .getResourceAsStream(resourcePath);
        Map<String, Object> menuMap = yaml.load(inputStream);

        if (menuMap != null) {
            ObjectMapper objectMapper = new ObjectMapper();
            for (Object obj : menuMap.values()) {
                menus.add(objectMapper.convertValue(obj, MenuItem.class));
            }
        }
    }

    public List<MenuItem> getMenuItems() {
        return menus;
    }

    public List<MenuItem> getLeftMenuItems() {
        return menus.stream().filter((m) -> (m.getLeft())).collect(Collectors.toList());
    }

    public List<MenuItem> getHeadMenuItems() {
        return menus.stream().filter((m) -> (m.getHead())).collect(Collectors.toList());
    }

    public List<MenuItem> getDashboardMenuItems() {
        return menus.stream().filter((m) -> (m.getDashboard())).collect(Collectors.toList());
    }

    public List<MenuItem> getToolbarMenuItems() {
        return menus.stream().filter((m) -> (m.getToolbar())).collect(Collectors.toList());
    }

    public List<MenuItem> getAdminMenuItems() {
        return menus.stream().filter((m) -> (m.getAdmin())).collect(Collectors.toList());
    }

}
