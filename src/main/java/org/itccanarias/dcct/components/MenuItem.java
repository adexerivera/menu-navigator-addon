package org.itccanarias.dcct.components;

import elemental.json.Json;
import elemental.json.JsonObject;

import java.io.Serializable;
import java.util.List;

public class MenuItem implements Serializable {
    private Integer index;
    private String link;
    private String title;
    private String icon;
    private Boolean left;
    private Boolean head;
    private Boolean dashboard;
    private Boolean admin;
    private Boolean toolbar;
    private List<MenuItem> children;

    public MenuItem() {
        this.left = false;
        this.head = false;
        this.dashboard = false;
        this.admin = false;
        this.toolbar = false;
    }

    public MenuItem(String link, String title) {
        this.left = false;
        this.head = false;
        this.dashboard = false;
        this.admin = false;
        this.toolbar = false;

        this.link = link;
        this.title = title;
    }

    public MenuItem(String link, String title, String icon) {
        this.left = false;
        this.head = false;
        this.dashboard = false;
        this.admin = false;
        this.toolbar = false;

        this.link = link;
        this.title = title;
        this.icon = icon;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Integer getIndex() {
        return index;
    }

    public String getLink() {
        return (this.admin) ? "admin/" + link : link;
    }

    public String getTitle() {
        return title;
    }

    public String getIcon() {
        return icon;
    }

    public Boolean getLeft() {
        return left;
    }

    public Boolean getHead() {
        return head;
    }

    public Boolean getDashboard() {
        return dashboard;
    }

    public Boolean getAdmin() {
        return admin;
    }

    public Boolean getToolbar() {
        return toolbar;
    }

    public List<MenuItem> getChildren() {
        return children;
    }

    @Override
    public String toString() {
        return "\nMenu{"
                + "\n\tid = " + index
                + ",\n\tlink = " + link
                + ",\n\ttitle = " + title
                + ",\n\ticon = " + icon
                + ",\n\tleft = " + left
                + ",\n\thead = " + head
                + ",\n\tdashboard = " + dashboard
                + ",\n\tadmin = " + admin
                + ",\n\ttoolbar = " + toolbar
                + "\n}";
    }

    /**
     * Converts Menu to a JsonObject and populates it with corresponding data.
     *
     * @return JsonObject filled with Menu data
     */
    public JsonObject toJsonObject() {
        JsonObject jsonMenu = Json.createObject();
        if (index != null)
            jsonMenu.put("index", index);
        jsonMenu.put("link", link);
        jsonMenu.put("title", title);
        if (icon != null)
            jsonMenu.put("icon", icon);
        return jsonMenu;
    }

}
