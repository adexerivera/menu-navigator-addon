package org.itccanarias.dcct.components;

import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.itccanarias.dcct.utils.Styles;

import java.util.List;

/**
 * @author arivera on 15/05/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 */
@HtmlImport("frontend://styles/mn-styles.html")
@HtmlImport("frontend://styles/mn-toolbar.html")
public class Toolbar extends HorizontalLayout {

    private Boolean useIcons = true;
    private ToolbarMenu toolbarMenu = new ToolbarMenu();

    public Toolbar() {
        configure();
    }

    public Toolbar(List<MenuItem> items) {
        configure();
        setToolbarMenuItems(items);
    }

    private void configure() {
        setClassName(Styles.toolbar);
        setPadding(false);
        setSpacing(false);
        add(toolbarMenu);
        add(toolbarMenu);
    }

    public ToolbarMenu getToolbarMenu() {
        return toolbarMenu;
    }

    public void setUseIcons(Boolean useIcons) {
        this.useIcons = useIcons;
    }

    public void setToolbarMenuItems(List<MenuItem> items) {
        setToolbarMenuItems(items, null);
    }

    public void setToolbarMenuItems(List<MenuItem> items, SubMenu parent) {
        for (MenuItem item : items) {
            if (item.getChildren() != null && item.getChildren().size() > 0) {
                SubMenu subMenuItems = new SubMenu(item.getTitle());
                if (useIcons) {
                    String stringIcon = (item.getIcon() == null) ? "LINES_LIST" : item.getIcon().toUpperCase();
                    VaadinIcon vIcon = VaadinIcon.valueOf(stringIcon);
                    subMenuItems.withIcon(vIcon);
                }
                if (parent == null) {
                    toolbarMenu.add(subMenuItems);
                } else {
                    parent.add(subMenuItems);
                }
                setToolbarMenuItems(item.getChildren(), subMenuItems);
            } else {
                if (parent == null) {
                    addToToolbar(item, toolbarMenu);
                } else {
                    addToToolbar(item, parent);
                }
            }
        }
    }

    private void addToToolbar(MenuItem item, MenuComponent toolbarMenu) {
        MenuNavButton menuNavButton = MenuNavButton.get()
                .withCaption(item.getTitle())
                .withNavigateTo(item.getLink());
        if (useIcons) {
            String stringIcon = (item.getIcon() == null) ? "LINES_LIST" : item.getIcon().toUpperCase();
            VaadinIcon vIcon = VaadinIcon.valueOf(stringIcon);
            menuNavButton.withIcon(vIcon);
        }
        toolbarMenu.add(menuNavButton);
    }

}
