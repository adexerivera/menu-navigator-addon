package org.itccanarias.dcct.components;

import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.itccanarias.dcct.utils.Styles;

import java.util.ArrayList;
import java.util.List;

/**
 * @author arivera on 16/05/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 */
@StyleSheet("frontend://styles/mn-board.css")
public class Board extends VerticalLayout {

    public Board() {
        configure();
    }

    public Board(List<MenuItem> items) {
        configure();
        add(createBoard(items));
    }

    public Board(List<MenuItem> items, String title) {
        configure();
        add(new H3(title));
        add(createBoard(items));
    }

    private void configure() {
        setPadding(false);
        setSpacing(false);
    }

    /**
     * Create board by dashboard items
     *
     * @param items Menu item array
     * @return Board
     */
    public Div createBoard(List<MenuItem> items) {
        Div board = new Div();
        List<Div> cards = createItemCards(items);
        board.addClassName(Styles.board);
        for (int i = 0; i < cards.size(); i += 4) {
            Div col0 = cards.get(i);
            Div col1 = (cards.size() > i + 1) ? cards.get(i + 1) : new Div();
            Div col2 = (cards.size() > i + 2) ? cards.get(i + 2) : new Div();
            Div col3 = (cards.size() > i + 3) ? cards.get(i + 3) : new Div();
            board.add(col0, col1, col2, col3);
        }
        return board;
    }

    /**
     * Create item cards to board
     *
     * @param items Board items
     * @return Board cards
     */
    private List<Div> createItemCards(List<MenuItem> items) {
        List<Div> result = new ArrayList<>();
        for (MenuItem item : items) {
            BoardCard boardCard = new BoardCard();
            result.add(boardCard.createCard(item));
        }
        return result;
    }
}
