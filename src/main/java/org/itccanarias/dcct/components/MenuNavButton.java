package org.itccanarias.dcct.components;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import org.itccanarias.dcct.utils.Styles;

import java.util.List;

public class MenuNavButton extends Button implements MenuComponent<MenuNavButton> {

    private String navigateTo = null;

    private Label toolTip = null;
    private Icon icon = null;
    private Icon otherIcon = null;

    public static MenuNavButton get() {
        return new MenuNavButton("");
    }

    /**
     * Only for the left menu
     * <p>
     * The caption is not displayed in the left menu
     *
     * @param caption
     */
    public MenuNavButton(String caption) {
        build(caption, null, null);
    }

    public MenuNavButton(Icon icon) {
        build(null, icon, null);
    }

    /**
     * Only for the left menu
     * <p>
     * The caption is not displayed in the left menu
     *
     * @param caption
     */
    public MenuNavButton(String caption, Icon icon) {
        build(caption, icon, null);
    }

    /**
     * Only for the left menu
     * <p>
     * The caption is not displayed in the left menu
     *
     * @param caption
     */
    public MenuNavButton(String caption, ComponentEventListener<ClickEvent<Button>> clickListener) {
        build(caption, null, clickListener);
    }

    public MenuNavButton(Icon icon, ComponentEventListener<ClickEvent<Button>> clickListener) {
        build(null, icon, clickListener);
    }

    /**
     * Only for the left menu
     * <p>
     * The caption is not displayed in the left menu
     *
     * @param caption
     */
    public MenuNavButton(String caption, Icon icon, ComponentEventListener<ClickEvent<Button>> clickListener) {
        build(caption, icon, clickListener);
    }

    private void build(String caption, Icon icon, ComponentEventListener<ClickEvent<Button>> clickListener) {
        withCaption(caption);
        withIcon(icon);
        if (clickListener != null) {
            withClickListener(clickListener);
        }
    }

    /**
     * Only for the left menu
     * <p>
     * The caption is not displayed in the left menu
     *
     * @param caption
     */
    public MenuNavButton withCaption(String caption) {
        super.setText(caption);
        return this;
    }

    public MenuNavButton withStyleName(String style) {
        getClassNames().add(style);
        return this;
    }

    public MenuNavButton withIcon(Icon icon) {
        if (this.icon != null) {
            remove(this.icon);
        }
        this.icon = icon;
        if (icon != null) {
            addToPrefix(icon);
        }
        return this;
    }

    public MenuNavButton withIcon(VaadinIcon icon) {
        return withIcon(icon.create());
    }

    public MenuNavButton withClickListener(ComponentEventListener<ClickEvent<Button>> clickListener) {
        super.addClickListener(clickListener);
        return this;
    }

    public MenuNavButton withDescription(String description) {
        super.getElement().setAttribute("title", description);
        return this;
    }

    public <T extends Component> MenuNavButton withNavigateTo(Class<T> viewClass) {
        withNavigateTo(UI.getCurrent().getRouter().getUrl(viewClass));
        return this;
    }

    public <T extends Component> MenuNavButton withNavigateTo(String link) {
        navigateTo = link;
        return this.withClickListener(e -> UI.getCurrent().navigate(navigateTo));
    }

    /**
     * Only for the left menu
     *
     * @param icon
     * @return
     */
    public MenuNavButton withOtherIcon(VaadinIcon icon) {
        return withOtherIcon(icon.create());
    }

    /**
     * Only for the left menu
     *
     * @param icon
     * @return
     */
    public MenuNavButton withOtherIcon(Icon icon) {
        if (otherIcon != null) {
            otherIcon.getElement().removeFromParent();
        }
        otherIcon = icon;
        otherIcon.getClassNames().add(Styles.buttonOtherIcon);
        addToSuffix(icon);
        return this;
    }

    public MenuNavButton withToolTip(String toolTip) {
        removeToolTip();
        this.toolTip = new Label(toolTip);
        this.toolTip.getClassNames().add(Styles.toolTip);
        addToSuffix(this.toolTip);
        return this;
    }

    public MenuNavButton withToolTip(int toolTip) {
        if (toolTip > 0) {
            withToolTip(String.valueOf(toolTip));
        } else {
            removeToolTip();
        }
        return this;
    }

    public Label getToolTip() {
        return toolTip;
    }

    public MenuNavButton removeToolTip() {
        if (toolTip != null) {
            toolTip.getElement().removeFromParent();
        }
        return this;
    }

    public boolean isActive() {
        return getClassNames().contains(Styles.active);
    }

    public MenuNavButton setActive(boolean active) {
        if (isActive() != active) {
            if (active) {
                getClassNames().add(Styles.active);
            } else {
                getClassNames().remove(Styles.active);
            }
        }
        return this;
    }

    public String getNavigateTo() {
        return navigateTo;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent add(MenuComponent c) {
        return null;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c) {
        return null;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index) {
        return null;
    }

    @Override
    public int count() {
        return 0;
    }

    @Override
    public <MenuComponent extends Component> MenuNavButton remove(MenuComponent c) {
        return null;
    }

    @Override
    public List<MenuComponent<?>> getList() {
        return null;
    }
}