package org.itccanarias.dcct.components;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Span;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.dom.Element;
import org.itccanarias.dcct.utils.Styles;

/**
 * @author arivera on 16/05/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 */
public class BoardCard extends Div {

    public BoardCard() {

    }

    public BoardCard(MenuItem menuItem) {
        createCard(menuItem);
    }

    /**
     * Create card div for boards
     *
     * @return Card item
     */
    public Div createCard(MenuItem item) {
        UI.getCurrent().getPage().addHtmlImport("bower_components/paper-card/paper-card.html");
        Div div = new Div();
        div.addClickListener(e -> goToHref(item.getLink()));
        div.addClassName(Styles.boardCard);
        Element card = new Element("paper-card");
        card.setAttribute("title", item.getTitle());
        Div info = new Div();
        info.setClassName("card-content");
        String stringIcon = (item.getIcon() == null) ? "LINES_LIST" : item.getIcon().toUpperCase();
        VaadinIcon vIcon = VaadinIcon.valueOf(stringIcon);
        Icon icon = new Icon(vIcon);
        icon.setSize("10%");
        Span title = new Span(item.getTitle());
        title.addClassName("mn-board-card-content-title");
        info.add(icon, title);
        Div actions = new Div();
        actions.setClassName("card-actions");
        Div goTo = new Div();
        goTo.add(new Span("Ir "), VaadinIcon.CHEVRON_CIRCLE_RIGHT.create());
        actions.add(goTo);
        card.appendChild(info.getElement(), actions.getElement());
        div.getElement().appendChild(card);
        return div;
    }

    /**
     * Navigate to href
     *
     * @param href String
     */
    private void goToHref(String href) {
        UI.getCurrent().navigate(href);
    }
}
