package org.itccanarias.dcct.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.itccanarias.dcct.utils.Styles;

import java.util.ArrayList;
import java.util.List;


/**
 * @author arivera on 15/05/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 */
@StyleSheet("frontend://styles/mn-left-menu.css")
public class LeftMenu extends VerticalLayout implements MenuComponent<VerticalLayout> {

    public LeftMenu() {
        super();
        setWidth("250px");
        setHeight("100%");
        getStyle().set("min-width", "250px");
        getStyle().set("max-width", "250px");
        getClassNames().add(Styles.leftMenu);
        setMargin(false);
        setPadding(false);
        setSpacing(false);
    }

    @Override
    public <MenuComponent extends Component> MenuComponent add(MenuComponent c) {
        if (c instanceof HasStyle) {
            ((HasStyle) c).getClassNames().add(Styles.leftMenuItem);
        }
        super.add(c);
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c) {
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index) {
        return c;
    }

    @Override
    public int count() {
        return getList().size();
    }

    @Override
    public <MenuComponent extends Component> LeftMenu remove(MenuComponent c) {
        super.remove(c);
        return this;
    }

    @Override
    public List<MenuComponent<?>> getList() {
        List<MenuComponent<?>> menuComponentList = new ArrayList<MenuComponent<?>>();
        for (int i = 0; i < getComponentCount(); i++) {
            Component component = getComponentAt(i);
            if (component instanceof MenuComponent<?>) {
                menuComponentList.add((MenuComponent<?>) component);
            }
        }
        return menuComponentList;
    }
}
