package org.itccanarias.dcct.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import org.itccanarias.dcct.utils.Styles;

import java.util.ArrayList;
import java.util.List;

/**
 * @author arivera on 15/05/2019
 * <p>
 * DCCT. Instituto Tecnológico de Canarias
 */
public class ToolbarMenu extends HorizontalLayout implements MenuComponent<HorizontalLayout> {

    public ToolbarMenu() {
        super();
        getClassNames().add(Styles.toolbarMenu);
        setMargin(false);
        setPadding(false);
        setSpacing(false);
        setJustifyContentMode(JustifyContentMode.START);
        getStyle().set("min-width", "50%");
        getStyle().set("max-width", "80%");
    }

    @Override
    public <MenuComponent extends Component> MenuComponent add(MenuComponent c) {
        if (c instanceof HasStyle) {
            ((HasStyle) c).getClassNames().add(Styles.toolbarMenuItem);
        }
        super.add(c);
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c) {
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index) {
        return c;
    }

    @Override
    public int count() {
        return getList().size();
    }

    @Override
    public <MenuComponent extends Component> ToolbarMenu remove(MenuComponent c) {
        super.remove(c);
        return this;
    }

    @Override
    public List<MenuComponent<?>> getList() {
        List<MenuComponent<?>> menuComponentList = new ArrayList<MenuComponent<?>>();
        for (int i = 0; i < getComponentCount(); i++) {
            Component component = getComponentAt(i);
            if (component instanceof MenuComponent<?>) {
                menuComponentList.add((MenuComponent<?>) component);
            }
        }
        return menuComponentList;
    }
}
