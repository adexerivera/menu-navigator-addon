package org.itccanarias.dcct.components;

import com.vaadin.flow.component.Component;

import java.util.List;

public interface MenuComponent<T extends Component> {
    <MenuComponent extends Component> MenuComponent add(MenuComponent c);

    <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c);

    <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index);

    int count();

    <MenuComponent extends Component> T remove(MenuComponent c);

    List<MenuComponent<?>> getList();
}
