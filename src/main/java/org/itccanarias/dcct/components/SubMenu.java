package org.itccanarias.dcct.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import org.itccanarias.dcct.utils.Styles;

import java.util.ArrayList;
import java.util.List;

public class SubMenu extends VerticalLayout implements MenuComponent<SubMenu> {

    private MenuNavButton button = new MenuNavButton("");
    private VerticalLayout content = new VerticalLayout();

    public static SubMenu get() {
        return new SubMenu("");
    }

    public SubMenu(String caption) {
        build(caption, null);
    }

    public SubMenu(Icon icon) {
        build(null, icon);
    }

    public SubMenu(String caption, Icon icon) {
        build(caption, icon);
    }

    private void build(String caption, Icon icon) {
        button = new MenuNavButton(caption, icon);
        button.withOtherIcon(VaadinIcon.ANGLE_RIGHT);
        button.addClickListener(e -> toggle());
        button.getClassNames().add(button.getClass().getSimpleName());
        button.getClassNames().add(Styles.leftMenuItem);
        content.setMargin(false);
        content.setPadding(false);
        content.setSpacing(false);
        super.setMargin(false);
        super.setPadding(false);
        super.setSpacing(false);
        super.getClassNames().add(Styles.subMenu);
        super.add(button, content);
    }

    public MenuNavButton getButton() {
        return button;
    }

    public SubMenu withCaption(String caption) {
        button.withCaption(caption);
        return this;
    }

    public SubMenu withIcon(Icon icon) {
        button.withIcon(icon);
        return this;
    }

    public SubMenu withIcon(VaadinIcon icon) {
        return withIcon(icon.create());
    }

    public SubMenu toggle() {
        if (isOpen()) {
            close();
        } else {
            open();
        }
        return this;
    }

    public SubMenu open() {
        getClassNames().add(Styles.open);
        button.setActive(true);
        return this;
    }

    public SubMenu close() {
        getClassNames().remove(Styles.open);
        button.setActive(false);
        return this;
    }

    public boolean isOpen() {
        return getClassNames().contains(Styles.open);
    }

    @Override
    public <MenuComponent extends Component> MenuComponent add(MenuComponent c) {
        if (this.getClassNames().contains(Styles.leftMenuItem) && c instanceof HasStyle) {
            ((HasStyle) c).getClassNames().add(Styles.leftMenuItem);
        }
        content.add(c);
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAsFirst(MenuComponent c) {
        return c;
    }

    @Override
    public <MenuComponent extends Component> MenuComponent addAt(MenuComponent c, int index) {
        return c;
    }

    @Override
    public <MenuComponent extends Component> SubMenu remove(MenuComponent c) {
        content.remove(c);
        return this;
    }

    @Override
    public int count() {
        return getList().size();
    }

    public List<MenuComponent<?>> getList() {
        List<MenuComponent<?>> menuComponentList = new ArrayList<MenuComponent<?>>();
        for (int i = 0; i < content.getComponentCount(); i++) {
            Component component = content.getComponentAt(i);
            if (component instanceof MenuComponent<?>) {
                menuComponentList.add((MenuComponent<?>) component);
            }
        }
        return menuComponentList;
    }
}